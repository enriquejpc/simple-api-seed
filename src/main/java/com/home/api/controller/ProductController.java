package com.home.api.controller;

import com.home.api.model.Product;
import com.home.api.service.ProductService;
import com.home.api.service.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/products")
public class ProductController {
    @Autowired
    private ProductService productService;

    @GetMapping
    public ResponseEntity<List<Product>> getAllProducts(){
        return ResponseEntity.ok(productService.getAll());
    }

    @PostMapping
    public ResponseEntity<Product> createProduct(@RequestBody Product product){
        return ResponseEntity.ok(this.productService.createProduct(product));
    }

    @PutMapping("/update")
    public ResponseEntity<Product> updateProduct(@RequestBody Product product){
        return ResponseEntity.ok(this.productService.updateProduct(product));
    }

    @PutMapping("/get/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable(name = "id", required = true) long id){
        return ResponseEntity.ok(this.productService.getProductById(id));
    }

    @DeleteMapping(name = "/delete/{id}")
    public HttpStatus deleteProductById(@PathVariable(name = "id", required = true) long id){
        this.productService.deleteProduct(id);
        return (HttpStatus.OK);
    }
}
