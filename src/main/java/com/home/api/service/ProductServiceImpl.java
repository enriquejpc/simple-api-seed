package com.home.api.service;

import com.home.api.exception.ResouceNotFoundException;
import com.home.api.model.Product;
import com.home.api.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    ProductRepository productRepository;

    @Override
    public Product createProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product updateProduct(Product product) {
        Optional<Product> optionalProduct = productRepository.findById(product.getId());
        if(optionalProduct.isPresent()){
            Product updateProduct = optionalProduct.get();
            updateProduct.setDescription(product.getDescription());
            updateProduct.setName(product.getName());
            productRepository.save(updateProduct);
            return product;
        }else {
            throw new ResouceNotFoundException("Product not found");
        }
    }

    @Override
    public List<Product> getAll() {
        return this.productRepository.findAll();
    }

    @Override
    public Product getProductById(long productId) {
        Optional<Product> optionalProduct = Optional.ofNullable(productRepository.findById(productId)
                .orElseThrow(() -> new ResouceNotFoundException("Product not found")));
        return optionalProduct.get();
    }

    @Override
    public void deleteProduct(long id) {
        Optional<Product> optionalProduct = Optional.ofNullable(productRepository.findById(id)
                .orElseThrow(() -> new ResouceNotFoundException("Product not found")));
        this.productRepository.deleteById(id);
    }
}
