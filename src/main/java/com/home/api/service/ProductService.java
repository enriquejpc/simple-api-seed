package com.home.api.service;

import com.home.api.model.Product;

import java.util.List;

public interface ProductService {

    Product createProduct(Product product);
    Product updateProduct(Product product);
    List<Product> getAll();
    Product getProductById(long productId);
    void deleteProduct(long id);

}
